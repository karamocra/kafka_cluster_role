
- [ansible-kafka](#ansible-kafka)
  - [Requirements](#requirements)
  - [Role Variables](#role-variables)
  - [Dependencies](#dependencies)
  - [Example Playbook](#example-playbook)


<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# ansible-kafka

An [Ansible](https://www.ansible.com) role to install/configure [Kafka](https://kafka.apache.org/)

## Requirements

-   [ansible-zookeeper](https://github.com/mrlesmithjr/ansible-zookeeper)

## Role Variables

[defaults/main.yml](defaults/main.yml)

## Dependencies

## Example Playbook

[playbook.yml](./playbook.yml)
